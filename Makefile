# Makefile for fftest
# For the moment it should be compiled in a folder inside q-e. For example inside: q-e/test-fft/
#
# mpif90 -g -D__MPI -cpp -fcheck=bounds fft_test.f90 -o fft_test.x ../Modules/mp_wave.o -I ../FFTXlib -I ../Modules -I ../UtilXlib  ../FFTXlib/libqefft.a -lfftw3 ../UtilXlib/libutil.a



CC = mpif90

CFLAGS = -debug full  -cpp -qopenmp -D__MPI -check bounds

INCLUDES = \
 -I/data/pdelugas/prova_fftxlib/include\
 -I/data/pdelugas/prova_qeMPLib/include 

OBJS = fft_test.o 

LIBS = /data/pdelugas/prova_fftxlib/lib/libqe_fftx.a /data/pdelugas/prova_qeMPLib/lib/libqeMPLib.a 

MAIN = fft_test

LFLAGS = 


all:	$(MAIN)

deafult:	$(MAIN)

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

$(MAIN).o: $(MAIN).f90 
	$(CC) $(CFLAGS) $(INCLUDES) -c $(MAIN).f90

.PHONY: clean

clean :
	- /bin/rm -f *.x *.o *.a *~ *_tmp.f90 *.d *.mod *.i *.L

# DO NOT DELETE
