!------------------------------------------------------------------------
module tools
!------------------------------------------------------------------------
 !
 contains
 !
SUBROUTINE read_rhog ( filename, ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
   !------------------------------------------------------------------------
   !! Read and distribute rho(G) from file  "filename".* 
   !! (* = dat if fortran binary, * = hdf5 if HDF5)
   !! Processor "root_in_group" reads from file, distributes to
   !! all processors in the intra_group_comm communicator 
      !
use iso_fortran_env, only : dp => real64
      !
      implicit none
      !
      character(len=*), intent(in)              :: filename
      !! name of file read (to which a suffix is added)
      integer,          intent(out)              :: ngm_g
      integer,          intent(out)              :: nspin
      !! read up to nspin components
      complex(dp), allocatable, intent(inout)   :: rho_g(:)
      !! temporary check while waiting for more definitive solutions
      integer, allocatable, intent(inout)       :: mill_g(:,:)
      logical, optional, intent(inout)             :: gamma_only
      !! if present, don't stop in case of open error, return a nonzero value
      integer, optional, intent(out)            :: ierr
      !
      real(dp), intent(out)                      :: b1(3), b2(3), b3(3)
      integer                                    :: iun, ns, ig
      logical                                    :: gamma_only_ 
      !
      !
      iun  = 4
      IF ( PRESENT(ierr) ) ierr = 0
      !
      !
      OPEN ( UNIT = iun, FILE = TRIM( filename ) // '.dat', &
           FORM = 'unformatted', STATUS = 'old', iostat = ierr )
      IF ( ierr /= 0 ) THEN
         ierr = 1
         return 
      END IF
      !
      ! ...  READING GAMMA_ONLY, NGM, SPIN
      !
      READ (iun, iostat=ierr) gamma_only_, ngm_g, nspin          
      !
      if (gamma_only_ ) then 
        gamma_only = .true.
      else 
        gamma_only = .false. 
      end if 
      PRINT *, gamma_only, ngm_g, nspin
      !
      IF ( ierr /= 0 ) THEN
         ierr = 2
         GO TO 10
      END IF
      !
      READ (iun, iostat=ierr) b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING B1 B2 B3'
      !
10    CONTINUE 
      !
      ALLOCATE( rho_g(ngm_g) )
      ALLOCATE (mill_g(3,ngm_g))
      !
      READ (iun, iostat=ierr) mill_g(1:3,1:ngm_g)
      IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING MILL_G'
      !
      DO ns = 1, 1
         !
         READ (iun, iostat=ierr) rho_g(1:ngm_g)
         IF ( ierr /= 0 ) WRITE(*,*) 'NOT READING RHO_G'
         !
!         IF ( ierr > 0 ) CALL errore ( 'read_rhog','error reading file ' &
!              & // TRIM( filename ), 2+ns )
         !
         ! 
!         IF ( nspin_ == 2 .AND. nspin == 4 .AND. ns == 2) THEN 
!            DO ig = 1, ngm 
!               rho(ig, 4 ) = rho(ig, 2) 
!               rho(ig, 2 ) = cmplx(0.d0,0.d0, KIND = DP) 
!               rho(ig, 3 ) = cmplx(0.d0,0.d0, KIND = DP)
!            END DO
!         END IF
      END DO
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE read_rhog
    !
    !
    !
SUBROUTINE write_rhog ( filename, b1, b2, b3, gamma_only, mill, rho, ecutrho )
      !------------------------------------------------------------------------
      !! Collects rho(G), distributed on "intra_group_comm", writes it
      !! together with related information to file "filename".*
      !! (* = dat if fortran binary, * = hdf5 if HDF5)
      !! Processor "root_in_group" collects data and writes to file
      !
USE ISO_FORTRAN_ENV, ONLY : DP => REAL64
      !
      IMPLICIT NONE
      !
      CHARACTER(LEN=*), INTENT(IN) :: filename
      !! rho(G) is distributed over this group of processors
      REAL(8),         INTENT(IN) :: b1(3), b2(3), b3(3)
      !!  b1, b2, b3 are the three primitive vectors in a.u.
      INTEGER,          INTENT(IN) :: mill(:,:)
      !! Miller indices for local G-vectors
      !! G = mill(1)*b1 + mill(2)*b2 + mill(3)*b3
      LOGICAL,          INTENT(IN) :: gamma_only
      !! if true, only the upper half of G-vectors (z >=0) is present
      COMPLEX(8),      INTENT(IN) :: rho(:)
      !! rho(G) on this processor
      REAL(8),OPTIONAL,INTENT(IN) :: ecutrho
      !! cut-off parameter for G-vectors, only the one in root node is
      !! used, hopefully the same as in the other nodes.  
      !
      INTEGER                  :: ngm, nspin, ns
      INTEGER                  :: iun, ierr
      !
      ngm  = SIZE (rho, 1)
!      IF (ngm /= SIZE (mill, 2) ) &
!         CALL errore('write_rhog', 'inconsistent input dimensions', 1)
      !
      nspin= 1 !SIZE (rho, 2)
      iun  = 6
      !
      ! ... find out the global number of G vectors: ngm
      !
      ierr = 0
      !
      OPEN ( UNIT = iun, FILE = TRIM(filename)//'.dat', &
                FORM = 'unformatted', STATUS = 'unknown', iostat = ierr )
      IF ( ierr /= 0 ) WRITE(*,*) 'error in opening the output file'
      !
      !
      WRITE (iun, iostat=ierr) gamma_only, ngm, nspin
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing gamma_only, ngm, nspin'
      !
      WRITE (iun, iostat=ierr) b1, b2, b3
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing b1, b2, b3'
      !
      ! ... write mill_g
      !
      WRITE (iun, iostat=ierr) mill(1:3,1:ngm)
      IF ( ierr /= 0 ) WRITE(*,*) 'not writing mill_g'
      !
      ! ... write rho_g
      !
      DO ns = 1, 1
         !
         WRITE (iun, iostat=ierr) rho(1:ngm)
         IF ( ierr /= 0 ) WRITE(*,*) 'not writing b1, b2, b3'
         !
      END DO
      !
      CLOSE (UNIT = iun, status ='keep' )
      !
      RETURN
      !
END SUBROUTINE write_rhog
    !
    !
    !
SUBROUTINE recips (a1, a2, a3, b1, b2, b3)
    !---------------------------------------------------------------------
    !
    !   This routine generates the reciprocal lattice vectors b1,b2,b3
    !   given the real space vectors a1,a2,a3. The b's are units of 2 pi/a.
    !
    !     first the input variables
    !
use iso_fortran_env, only : dp => real64
    !
    !
    implicit none
    !
    real(dp), intent (in) :: a1 (3), a2 (3), a3 (3)
    real(dp), intent (out)  :: b1 (3), b2 (3), b3 (3)
    ! input: first direct lattice vector
    ! input: second direct lattice vector
    ! input: third direct lattice vector
    ! output: first reciprocal lattice vector
    ! output: second reciprocal lattice vector
    ! output: third reciprocal lattice vector
    !
    !   then the local variables
    !
    real(dp),parameter  :: pi = 4._dp * atan(1._dp) 
    real(dp)            :: vol 
    vol  = dot_product(a1, cross(a2,a3)) 
    b1 = 2 * pi / vol * cross(a2, a3) 
    b2 = 2 * pi / vol * cross(a3, a1) 
    b3 = 2 * pi / vol * cross(a1,a2) 
end subroutine recips 


function volume(a1,a2,a3) result(v) 
  use iso_fortran_env, only: dp => real64
  implicit none
  real(dp) :: a1(3), a2(3), a3(3) 
  real(dp) :: v 
  v = dot_product(a1, cross(a2,a3)) 
end function volume 

function cross(v1, v2) result(w) 
  use iso_fortran_env, only: dp=>real64
  implicit none 
  real(dp) :: w(3) 
  real(dp) :: v1(3), v2(3) 
  w(1) = v1(2)*v2(3) - v1(3)*v2(2)
  w(2) = v1(3)*v2(1) - v1(1)*v2(3) 
  w(3) = v1(1)*v2(2) - v1(2)*v2(1) 
end function cross   
!
!----------------------------------------------------------------------
end module tools
!----------------------------------------------------------------------
!
!
!-----------------------------------------------------------------------
program test_charge_density
  !-----------------------------------------------------------------------
  !
  !
  use iso_fortran_env, only : dp => real64
  use tools, only: read_rhog, write_rhog, recips, volume
  use fft_types, only: fft_type_descriptor, fft_type_init, fft_stick_index
  use stick_base, only: sticks_map
  use fft_interfaces, only: invfft, fwfft
  use fft_ggen, only : fft_set_nl
  !
  !... para libraries
  !
  use mpi
  use mp,         only : mp_size, mp_rank, mp_bcast, mp_sum, mp_start, mp_barrier
  use mp_wave,    only : splitwf, mergekg, mergewf
  !
  implicit none
  !
!new allocated variabiles
  integer                   :: ngm_g, ngm_g_new, ngm2_g
  integer                   :: ngm_offset, ngm2_offset
  integer                   :: nspin
  complex(dp), allocatable  :: rho_g(:), rho(:)
  complex(dp), allocatable  :: psic(:)
  integer, allocatable      :: mill_g(:,:), mill(:,:), mill2(:,:)
  integer, allocatable      :: ig_l2g(:), ig_l2g_2(:)
  integer, allocatable      :: ngmpe(:), ngmpe2(:)
  logical                   :: gamma_only = .true., is_in_mesh= .false.
  integer                   :: ierr = 0
  integer                   :: un, i, ig, countg, countg_g
  integer                   :: ng, ngm
  integer                   :: nr2b2, nr1b2, nr3b2
  real(dp)                  :: gcutm
  real(dp)                  :: b1(3), b2(3), b3(3)
  real(dp)                  :: a(3,3)
  real(dp)                  :: bg(3,3)
  real(dp)                  :: gmax(3)
  real(dp), allocatable     :: g(:,:)
  !
  !... fft descriptors
  !
  type(fft_type_descriptor) :: dfftp, dfftp2
  type(sticks_map) :: smap, smap2
  !
  ! ... para variables 
  !
  integer :: mype, npes, comm, root
  !! mpi handles
  integer :: ntgs
  !! number of taskgroups
  integer :: nbnd
  !! number of bands
  logical :: iope
  !!
  logical :: lpara, is_local
  !
  !
  !
  ! Initialize MPI and internal parallelization parameters
call initialize_mp() 
!
! ... now read the charge density and wave function parameters from a .dat file
!
call read_and_allocate_rho_from_file() 
!
! initialize fftxlib handles for the original and derived grid 
! if the original grid was gamma_only-type we derive a full grid and vice-versa
!
call initialize_fftxlib()   
!
! compute distributed miller indexes and drop global one. 
! 
call compute_distributed_mi() 
!
call compute_local_to_global_indexing()  
deallocate(mill)
  ! ...  split rho_g
  !
  allocate(rho(dfftp%ngm))
  call splitwf ( rho, rho_g, dfftp%ngm, ig_l2g, mype, dfftp%nproc, 0, mpi_comm_world )
  !
  !
  allocate (psic(dfftp%nnr))
  !
  psic = cmplx(0.d0,0.d0,kind=dp)
  !
  !
  do i = 1, dfftp%ngm
      !
      if(gamma_only) then
              psic(dfftp%nlm(i))=dconjg(rho(i))
      end if
      !
      psic(dfftp%nl(i))=rho(i)
      !
  end do
  !
  !
  call invfft('Rho', psic, dfftp)
  !
  call fwfft('Rho', psic, dfftp2)
  !
!  call invfft('rho', psic, dfftp2)
  !
!  call fwfft('rho', psic, dfftp)
  !
  !
  deallocate(rho)
  allocate(rho(dfftp2%ngm))
  !
  do i = 1, dfftp2%ngm
      !
      rho(i)=psic(dfftp2%nl(i))
      !
  end do
  !
  !
  deallocate(rho_g)
  if (mype == 0) then 
    allocate(mill_g(3,ngm2_g), rho_g(ngm2_g))
  else 
    allocate(mill_g(1,1), rho_g(1))
  end if
  !
  call mergekg ( mill2, mill_g, dfftp2%ngm, ig_l2g_2, mype, dfftp2%nproc, 0, mpi_comm_world )
  !
  call mergewf ( rho, rho_g, dfftp2%ngm, ig_l2g_2, mype, dfftp2%nproc, 0, mpi_comm_world )
  if (mype == 0) print *, 'rho_g', rho_g(1) 
  !
  !if ( mype == 0) call  write_rhog ( "output-charge-2", b1, b2, b3, .not. gamma_only, mill_g(:,1:countg_g), rho_g(1:countg_g) )
  if ( mype == 0) call  write_rhog ( "output-charge", b1, b2, b3, .not. gamma_only, mill_g(:,1:countg_g), rho_g(1:countg_g) )
  !
  !
  if (allocated(rho_g))  deallocate( rho_g ) 
  if (allocated(mill_g)) deallocate( mill_g )
  !
  call mpi_finalize(ierr)
 !
contains 
  subroutine initialize_mp() 
#if defined(__MPI)
    call mpi_init(ierr)
    call mpi_comm_rank(mpi_comm_world, mype, ierr)
    call mpi_comm_size(mpi_comm_world, npes, ierr)
    call mp_start(npes, mype, mpi_comm_world) 
    comm = mpi_comm_world
    root = 0
    if (mype == root) then
      iope = .true.
    else
      iope = .false.
    endif
    lpara=.true. 
    if ( mype == 0 ) print '("test with ",i6," mpi ranks")', npes
#else
    mype = 0
    npes = 1
    comm = 0
    ntgs = 1
    root = 0
    iope = .true.
    lpara=.false.
#endif
  end subroutine initialize_mp
  !
  subroutine read_and_allocate_rho_from_file() 
    !
    if ( mype == 0 ) then 
      call read_rhog ( "charge-density", ngm_g, nspin, rho_g, mill_g, b1, b2, b3, gamma_only, ierr )
      call errore ("error reading charge-density file", "ciao", ierr) 
    end if 
    !
    call mp_bcast(ngm_g, 0, mpi_comm_world)
    call mp_bcast(nspin, 0, mpi_comm_world)
    call mp_bcast(gamma_only, 0, mpi_comm_world)
    call mp_bcast(b1, 0, mpi_comm_world)
    call mp_bcast(b2, 0, mpi_comm_world)
    call mp_bcast(b3, 0, mpi_comm_world)
   !
   !
    if ( mype .ne. 0)  allocate (mill_g(3,ngm_g), rho_g(1))
   !
    call mp_bcast(mill_g, 0, mpi_comm_world)
    ! 
    !
    bg(:,1)=b1
    bg(:,2)=b2
    bg(:,3)=b3
    !
    !
    ! ... g is the array for the grid indices
    !
    !
    if ( mype == 0) then
     ! 
      gmax(1:3) = mill_g(1,ngm_g) * bg(:, 1) + mill_g(2,ngm_g) * bg(:, 2) + mill_g(3,ngm_g) * bg(:, 3)
      !
      gcutm = dot_product(gmax(:),gmax(:)) + 0.001
      !
      !
      nr1b2 = maxval(abs(mill_g(1,:)))
      nr2b2 = maxval(abs(mill_g(2,:)))
      nr3b2 = maxval(abs(mill_g(3,:)))
      !
    end if
    !
    call mp_bcast(gcutm, 0, mpi_comm_world)
    call recips( b1, b2, b3, a(:,1), a(:,2), a(:,3))
    if ( mype== 0) then
      !
      print '(3f12.6)', b1  
      print '(3f12.6)', b2
      print '(3f12.6)', b3
      print '("volume ", f8.3)', volume(a(:,1),a(:,2),a(:,3))
      print '("total charge per cell  ",f6.3)', volume(a(:,1), a(:,2), a(:,3))*rho_g(1)%re
      !
    end if
  end subroutine read_and_allocate_rho_from_file
  !
  subroutine initialize_fftxlib() 
    ! 
    dfftp%rho_clock_label='fftp'
    dfftp2%rho_clock_label='fftp2'
    !
    call fft_type_init(dfftp, smap, "rho", gamma_only, lpara, comm, a, bg, gcutm, 4.d0, nyfft=1, nmany=1)
    !
    call fft_type_init(dfftp2, smap2, "rho", .not. gamma_only, lpara, comm, a, bg, gcutm, 4.d0, nyfft=1, nmany=1)
    !
    allocate(ngmpe(npes),ngmpe2(npes))
    !
    ! store in ngmpe and ngmpe2 the number of G vectors operated on each node
    ngmpe  = 0
    ngmpe2 = 0
    !
    ngmpe( mype + 1 )  = dfftp%ngm
    ngmpe2( mype + 1 ) = dfftp2%ngm
    !
    print '("G vectors of original grid assingned to process ", i3, " are: ",i7)', mype, ngmpe(mype+1) 
    print '("G vectors of derived  grid assingned to process ", i3, " are: ",i7)', mype, ngmpe2(mype+1)
    call mp_barrier(comm)
    !
    call mp_sum( ngmpe, comm )
    call mp_sum( ngmpe2, comm )
    !
    !
    ngm_g_new = sum(ngmpe) 
    ngm2_g = sum(ngmpe2)
    !
    !
    ngm_offset = 0
    ngm2_offset = 0
    !
    !
    do ng = 1, mype
      ngm_offset = ngm_offset + ngmpe( ng )
      ngm2_offset = ngm2_offset + ngmpe2( ng )
    end do
    !
    deallocate( ngmpe, ngmpe2 )
  end subroutine initialize_fftxlib 
  ! 
  subroutine compute_distributed_mi() 
    ! 
    allocate( ig_l2g( dfftp%ngm), ig_l2g_2( dfftp2%ngm ) )
    allocate(mill(3,dfftp%ngm), mill2(3,dfftp2%ngm))
    !
    ngm = 0
    countg= 0
    countg_g= 0
    do ig = 1,ngm_g
      if ( fft_stick_index( dfftp, mill_g(1,ig), mill_g(2,ig) ) .gt. 0 ) then
        ngm = ngm + 1
        ig_l2g( ngm ) = ig 
        mill(:,ngm) = mill_g(:,ig)
      end if
      !
      if (.not. gamma_only) then
        !
        is_in_mesh = mill_g(1,ig).gt.0
        is_in_mesh = is_in_mesh .or. ( mill_g(1,ig).eq.0 .and. mill_g(2,ig).gt.0 )
        is_in_mesh = is_in_mesh .or. ( mill_g(1,ig).eq.0 .and. mill_g(2,ig).eq.0 .and. mill_g(3,ig).ge.0)
        !
        if(is_in_mesh) countg_g = countg_g + 1
        !
      else
        ! 
        is_in_mesh = ( mill_g(1,ig).eq.0 .and. mill_g(2,ig).eq.0 .and. mill_g(3,ig).eq.0)
        countg_g = countg_g +1
        !
      end if       
      !  
      !
      if (.not. gamma_only) then 
        if ( fft_stick_index( dfftp2, mill_g(1,ig), mill_g(2,ig) ) .gt. 0 ) then 
           !
           if( is_in_mesh ) then
              countg=countg+1
              mill2(:,countg) = mill_g(:,ig)
              ig_l2g_2(countg ) =  countg_g
           end if           
        end if
      else
        if ( fft_stick_index( dfftp2, mill_g(1,ig), mill_g(2,ig) ) .gt. 0 ) then
           countg=countg+1
           mill2(:,countg) = mill_g(:,ig)
           ig_l2g_2(countg ) =  countg_g
        end if
        if (.not. is_in_mesh) countg_g = countg_g+1 
        if (.not. is_in_mesh .and. ( fft_stick_index( dfftp2, -mill_g(1,ig), -mill_g(2,ig) ) .gt. 0 )  ) then
          countg   = countg+1 
          mill2(:,countg) = - mill_g(:,ig)
          ig_l2g_2(countg ) =  countg_g
        end if
      end if
    end do 
#if defined(__DEBUG)
    print '("check: mpype= ",i0,"   ngm2_g = ",i0,"  countg_g=", i0)', mype, ngm2_g, countg_g
    !
#endif
    deallocate(mill_g)
    ! 
  end subroutine compute_distributed_mi 
  ! 
  subroutine compute_local_to_global_indexing() 
    ! 
    allocate(g(3,dfftp%ngm))
    !
    do ig=1,dfftp%ngm
      g(1:3,ig) = mill(1,ig) * bg(:, 1) + mill(2,ig) * bg(:, 2) + mill(3,ig) * bg(:, 3)
    end do
    !
    g = g / 8.d0 / atan(1._dp)
    call fft_set_nl( dfftp, a, g) 
   !
    deallocate(g)
    allocate(g(3,dfftp2%ngm))
    g = 0.d0
    !
    do ig=1,dfftp2%ngm
       g(1:3,ig) = mill2(1,ig) * bg(:, 1) + mill2(2,ig) * bg(:, 2) + mill2(3,ig) * bg(:, 3)  
    end do
    !
    g = g / 8.d0 / atan(1.d0)
   !
    call fft_set_nl( dfftp2, a, g(:,1:dfftp2%ngm))
    !
  end subroutine compute_local_to_global_indexing 

end program test_charge_density
!
